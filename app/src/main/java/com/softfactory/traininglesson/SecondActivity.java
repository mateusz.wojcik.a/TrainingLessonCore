package com.softfactory.traininglesson;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by rafaldziuryk on 28/11/17.
 */

public class SecondActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        textView = findViewById(R.id.textview2);

        String data = getIntent().getStringExtra("key");
        if (data != null) {
            textView.setText(data);
        }
    }
}
